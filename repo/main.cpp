using namespace std;
#include <iostream>
#include "Vector.h"

void main()
{
	Vector v1(2);
	int size = v1.size();
	int capacity = v1.capacity();
	int resizeFactor = v1.resizeFactor();
	std::cout << "The size is: " << size << std::endl;
	std::cout << "The capacity is: " << capacity << std::endl;
	std::cout << "The resizeFactor is: " << resizeFactor << std::endl;
	std::cout << "Is empty? : " << v1.empty() << std::endl;
	v1.push_back(1);
	std::cout << "Push 1... Is empty? : " << v1.empty() << std::endl;
	v1.push_back(2);
	std::cout << "Push 2...Is empty? : " << v1.empty() << std::endl;
	v1.push_back(3);
	std::cout << "Push 3...Is empty? : " << v1.empty() << std::endl;
	v1.push_back(4);
	std::cout << "Push 4...Is empty? : " << v1.empty() << std::endl;

	/*v1.pop_back();
	std::cout << "Pop... Is empty? : " << v1.empty() << std::endl;
	v1.pop_back();
	std::cout << "Pop... Is empty? : " << v1.empty() << std::endl;
	v1.pop_back();
	std::cout << "Pop... Is empty? : " << v1.empty() << std::endl;
	v1.pop_back();
	std::cout << "Pop... Is empty? : " << v1.empty() << std::endl;
	v1.reserve(11);
	*/
	//Vector v2(v1);
	//std::cout << "copy...." << v1.empty() << std::endl;
	
	Vector v2(4);
	v2 = v1;
	
	
	std::cout << "v2[3] (Should be 4) = " << v2[3] << std::endl;

	v1.~Vector();
	v2.~Vector();

	system("PAUSE");
}