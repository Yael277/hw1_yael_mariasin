using namespace std;
#include <iostream>
#include "Vector.h"

//A

Vector::Vector(int n) 
{
	if (n < 2)
	{
		n = 2;
	}

	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = -1;
	this->_elements = new int[n];
}

Vector::~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
		this->_elements = NULL;
		this->_size = 0;		//add initialization
		this->_capacity = 0;	//add initialization
	}
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return (this->_size < 0);
}

//B

void Vector::push_back(const int& val)
{
	if (this->_size + 1 == this->_capacity)
	{
		int* copyOldElements = this->_elements;
		this->_elements = new int[this->_capacity + this->_resizeFactor];
		this->_capacity += this->_resizeFactor;
		//copy old elements to the new place
		for (int i = 0; i <= this->_size; i++)
		{
			this->_elements[i] = copyOldElements[i];
		}
		delete[] copyOldElements;
		copyOldElements = NULL;
	}
	this->_size++;
	this->_elements[this->_size] = val;
}

int Vector::pop_back()
{
	int ans = 0;

	if (this->_size >= 0)
	{
		ans = this->_elements[this->_size];
		this->_size--;
	}
	else
	{
		std::cout << "error: pop from empty vector" << std::endl;
		ans = -9999;
	}
	return ans;
}

void Vector::reserve(int n)
{
	if (n > this->_capacity)
	{
		int* copyOldElements = this->_elements;
		int tempCapacity = this->_capacity + this->_resizeFactor;
		while(n < tempCapacity)
		{
			tempCapacity += this->_resizeFactor;
		}

		this->_capacity = tempCapacity;
		this->_elements = new int[tempCapacity];

		for (int i = 0; i <= this->_size; i++)
		{
			this->_elements[i] = copyOldElements[i];
		}

		delete[] copyOldElements;
		copyOldElements = NULL;
	}
}

void Vector::resize(int n)
{
	if (n < this->_capacity)
	{
		while (n < this->_capacity)
		{
			this->pop_back();
		}
		this->_capacity = n;
	}
	else if (n > this->_capacity)
	{
		reserve(n);
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i <= this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	resize(n);
	this->_size = this->_capacity;
	assign(val);
}

//C

Vector::Vector(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = new int[this->_capacity];

	for (int i = 0; i <= this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}

Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements;

	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = new int[this->_capacity];

	for (int i = 0; i <= this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	
	return *this;
}

//D

int& Vector::operator[](int n) const
{
	int ans = 0;

	if (n > this->_size)
	{
		std::cout << "The place you entered is not in the range of existing locations..." << std::endl;
		if (this->_size >= 0)
		{
			ans = this->_elements[0];
		}
		//else the ans would be 0
	}
	else
	{
		ans = this->_elements[n];
	}

	return ans;
}

